#!/usr/bin/env bash

esc=""
boldon="${esc}[1m";
boldoff="${esc}[22m"
redf="${esc}[31m";
bluef="${esc}[34m";
reset="${esc}[0m"

i=0
while read line
do
  line=${line##*/}
  array[ $i ]=${line%.svg}
  (( i++ ))
done < <(ls svg/*.svg)

for i in ${array[@]};
do
  inkscape --export-type="png" ./svg/$i.svg --export-filename=./png40/$i.png
done
