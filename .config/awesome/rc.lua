-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
-- Notification library
local naughty = require("naughty")

modkey = "Mod4"
terminal = "alacritty"
editor = os.getenv("EDITOR") or "emacs"
editor_cmd = terminal .. " -e " .. editor

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

awesome.set_preferred_icon_size(32)
beautiful.init(os.getenv("HOME") .. "/.config/awesome/themes/earth-close/theme.lua")

local layouts =  {

    awful.layout.suit.tile,               -- 1:
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,

    --awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,

    --awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,     --2

    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,

    awful.layout.suit.corner.nw,           -- 3:

    awful.layout.suit.floating,           -- 4:
--  awful.layout.suit.corner.ne,
--  awful.layout.suit.corner.sw,
--  awful.layout.suit.corner.se,
  }
awful.layout.layouts = layouts

local tags = {}

  local tagpairs = {
--  names  = { "term", "net", "edit", "place", 5, 6, 7, 8, 9 },
    names  = {
      " 1 Browse ", " 2 Code ", " 3 Test ",
      " 4 Scribble ", " 5 Shell", " 6 Admin ", " 7 Other "
      --" ❶ Browse ", " ❷ Code ", " ❸ Test ",
      --" ❹ Scribble ", " ❺ Admin ", " ❻ Other "
      --" ❻ 六 ", " ❼ 七 ", " ❽ 八 ", " ❾ 九 "
    },

    layout = {
      layouts[1], layouts[1], layouts[2],
      layouts[1], layouts[3], layouts[1],
      layouts[1],
    }
  }

  awful.screen.connect_for_each_screen(function(s)
    -- Each screen has its own tag table.
    tags[s] = awful.tag(tagpairs.names, s, tagpairs.layout)
  end)

local hotkeys_popup_widget = require("awful.hotkeys_popup").widget
local M = {}
--local debian = require("debian.menu")

M.awesome = {
  { "hotkeys", function()
      hotkeys_popup_widget.show_help(nil, awful.screen.focused())
    end },
  { "manual", terminal .. " -e man awesome" },
  { "edit config", editor_cmd .. " " .. awesome.conffile },
  { "Terminal", terminal },
  { "Shutdown/Logout", "oblogout" },
  { "restart", awesome.restart },
  { "quit", function() awesome.quit() end }
}

M.browsers = {
--  { "LibreWolf", "librewolf" },
  { "Brave", "brave" },
  { "Vivaldi", "vivaldi" },
  { "firefox", "firefox", awful.util.getdir("config") .. "/firefox.png" },
}

M.code = {
  { "PhpStorm", "phpstorm" },
  { "TablePlus", "tableplus" },
  { "DBeaver", "dbeaver" },
}

M.favorite = {
  { "Shutter", "shutter" },
  { "Inkscape - Drawing Tool", "Inkscape" },
  { "libreoffice", "libreoffice" },
  { "Obsidian", "sh -c '~/Applications/Obsidian-0.13.23.AppImage'" },
  { "3C Protfolio Manager", "sh -c '~/Applications/3c-portfolio-manager.AppImage'" },
  { "Rambox", "rambox" },
  { "nuclear", "nuclear" },
}

M.tools = {
  { "Enpass", "/opt/enpass/Enpass" },
  { "ARandR - Display setup", "arandr"},
  { "xScreensaver", "xscreensaver-demo"},
  { "pavucontrol", "pavucontrol"},
--  { "deluge torrent client", "deluge-gtk" }
  { "lxappearance - GTK+ theme switcher", "lxappearance"},
}

M.system = {
  { "open terminal", terminal },
  { "system monitor - bpytop", terminal .. " -e bpytop" },
  { "lock screen", "xscreensaver-command -lock" },
}

local menu_items = {
  { "awesome", M.awesome, beautiful.awesome_subicon },
  { "browsers", M.browsers },
  { "favorite", M.favorite },
  { "tools", M.tools },
--	{ "Debian", debian.menu.Debian_menu.Debian },
  { "system", M.system },
}

local main_menu = awful.menu({ items = menu_items })

menubar.utils.terminal = terminal
mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = main_menu })

local globalbuttons = gears.table.join(
    awful.button({ }, 3, function () main_menu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
  )

local clientbuttons = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
  )

local globalkeys = gears.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () main_menu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    -- Resize
    awful.key({ modkey, "Control" }, "Down",
              function () awful.client.moveresize( 0, 0, 0, -20) end),
    awful.key({ modkey, "Control" }, "Up",
              function () awful.client.moveresize( 0, 0, 0,  20) end),
    awful.key({ modkey, "Control" }, "Left",
              function () awful.client.moveresize( 0, 0, -20, 0) end),
    awful.key({ modkey, "Control" }, "Right",
              function () awful.client.moveresize( 0, 0,  20, 0) end),

    -- Move
    awful.key({ modkey, "Shift"   }, "Down",
              function () awful.client.moveresize(  0,  20,   0,   0) end),
    awful.key({ modkey, "Shift"   }, "Up",
              function () awful.client.moveresize(  0, -20,   0,   0) end),
    awful.key({ modkey, "Shift"   }, "Left",
              function () awful.client.moveresize(-20,   0,   0,   0) end),
    awful.key({ modkey, "Shift"   }, "Right",
              function () awful.client.moveresize( 20,   0,   0,   0) end),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Dmenu
    awful.key({ modkey },            "r",
    	function ()
    		awful.util.spawn("dmenu_run") end,
    		{decription = "Run dmenu", group = "launcher"}),

    -- Browser
    awful.key({ modkey }, "v",
    	function()
    		awful.util.spawn('vivaldi-stable') end,
    		{description = "Vivaldi Browser", group = "launcher"}),

    -- PhpStorm
    awful.key({ modkey }, "c",
    	function()
    		awful.util.spawn('phpstorm') end,
    		{description = "PhpStorm", group = "launcher"}),

    -- File explorer
    awful.key({ modkey }, "e",
    	function()
    		awful.util.spawn('pcmanfm') end,
    		{description = "File-Manager", group = "launcher"}),

    -- File explorer
    awful.key({ modkey, "Shift" }, "e",
    	function()
    		awful.util.spawn('/opt/enpass/Enpass') end,
    		{description = "Enpass", group = "launcher"}),

    -- emacs
    awful.key({ modkey }, "d",
    	function()
    		awful.util.spawn("emacsclient -c -a 'emacs'") end,
    		{description = "doom-emacs", group = "launcher"}),

		-- notepadqq
		awful.key({ modkey, "Control" }, "o",
    	function()
    		awful.util.spawn('notepadqq') end,
    		{description = "notepadqq", group = "launcher"}),

    -- Obsidian
    awful.key({ modkey, "Shift" }, "o",
    	function()
    		awful.spawn.with_shell("sh -c '~/Applications/Obsidian-0.13.23.AppImage'") end ,
    		{description = "Obsidian", group = "launcher"}),

    -- Toggle microphone state
--    awful.key({ modkey, "Shift" }, "m",
--              function ()
--                  --beautiful.mic:toggle()
--                  awful.spawn.with_shell("amixer set Capture toggle")
--              end,
--              {description = "Toggle microphone (amixer, takes 5sec.)", group = "Hotkeys"}
--    ),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),

    -- Screenshots with shutter
    awful.key({ modkey, "Shift" }, "p",
    	function()
    		awful.util.spawn('shutter') end,
    		{description = "Shutter", group = "launcher"})

  )

local clientkeys = gears.table.join(

    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}),
    awful.key({ modkey, "Control" }, "m",
        function (c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end ,
        {description = "(un)maximize vertically", group = "client"}),
    awful.key({ modkey, "Shift"   }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end ,
        {description = "(un)maximize horizontally", group = "client"})
  )

for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
      -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
  end

awful.rules.rules = {

    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },

--     { rule = { class = "vivaldi-stable" },
--         properties = { screen = 2, tag = " ❶ Browse " }
--     },
    { rule = { class = "firefox" },
        properties = { tag = screen[1].tags[3] }
    },
    --{ rule = { class = "phpstorm" },
    --    properties = { tag = screen[1].tags[2] }
    --},
    { rule = { class = "vivaldi-stable" },
        properties = { tag = screen[1].tags[1] }
    },
    { rule = { class = "enpass" },
        properties = { tag = screen[1].tags[6] }
    },
    { rule_any = { instance = { "mtui"} },
        properties = {  placement = awful.placement.centered , floating = true },
    }
--    { rule = { class = "alacritty" },
--        properties = { tag = screen[1].tags[2] }
--    },
--     { rule = { class = "phpstorm" },
--         properties = { screen = 2, tag = " ❷ Code " }
--     },
--     { rule = { class = "alacritty" },
--         properties = { screen = 1, tag = " ❷ Code " }
--     },

  }

root.buttons(globalbuttons)
root.keys(globalkeys)

client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c)
    c.border_color = beautiful.border_focus
    c.opacity = 1 end)
client.connect_signal("unfocus", function(c)
    c.border_color = beautiful.border_normal
    c.opacity = 0.85 end)

client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button({ }, 1, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            c:emit_signal("request::activate", "titlebar", {raise = true})
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

function set_wallpaper(s)
  -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

local taglist = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),
    awful.button({ modkey }, 1, function(t)
                              if client.focus then
                                  client.focus:move_to_tag(t)
                              end
                          end),
    awful.button({ }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, function(t)
                              if client.focus then
                                  client.focus:toggle_tag(t)
                              end
                          end),
    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
  )

local tasklist = gears.table.join(
	awful.button({ }, 1, function (c)
		if c == client.focus then
			c.minimized = true
		else
			c:emit_signal(
				"request::activate",
				"tasklist",
				{raise = true}
			)
		end
	end),
	awful.button({ }, 3, function()
		awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({ }, 4, function ()
		awful.client.focus.byidx(1)
	end),
	awful.button({ }, 5, function ()
		awful.client.focus.byidx(-1)
	end)
  )

local WB = {}
wibox_package = WB -- global object name

--local launcher = awful.widget.launcher(
--  { image = beautiful.awesome_icon, menu = main_menu }
--)

--mykeyboardlayout = awful.widget.keyboardlayout()

myspacer = wibox.widget.textbox("  ")
myspacerline = wibox.widget.textbox(" | ")

mytextclock = wibox.widget.textclock()

local volume_control = require("volume-control")
volumecfg = volume_control({
    device = nil,
	step = '2%',
	lclick  = "pavucontrol",
    widget_text = {
        on  = '% 3d%%',        -- three digits, fill with leading spaces
        off = '% 3dM',
    },
})

local battery_widget = require("battery-widget")
local BAT0 = battery_widget {
    adapter = "BAT0",
    ac = "AC",
    battery_prefix = "Bat1: ",
    widget_text = "${AC_BAT}${color_on}${percent}%${color_off}"
}
local BAT1 = battery_widget {
    adapter = "BAT1",
    ac = "AC",
    battery_prefix = "Bat2: ",
    widget_text = "${AC_BAT}${color_on}${percent}%${color_off} | Vol:"
}

--local mic_widget = require("widgets/mic")
--mymic = mic_widget({
--    timeout = 10,
--    settings = function(self)
--        if self.state == "muted" then
--            self.widget:set_image(os.getenv("HOME") .. "/.config/awesome/themes/default/icons/fontawesome/microphone-slash.png")
--        else
--            self.widget:set_image(os.getenv("HOME") .. "/.config/awesome/themes/default/icons/fontawesome/microphone.png")
--        end
--    end
--})

--beautiful.mic = mymic
----local widget_mic = wibox.widget { mymic.widget, layout = wibox.layout.align.horizontal }
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

function WB.add_widgets_left (s)
    return { -- Left widgets
         layout = wibox.layout.fixed.horizontal,
         --launcher,
         s.mytaglist,
         s.mypromptbox,
        myspacerline,
     }
end

function WB.add_widgets_middle (s)
  return s.mytasklist -- Middle widget
end

function WB.add_widgets_right_main (s)
    return { -- Right widgets
         layout = wibox.layout.fixed.horizontal,
         myspacer,
         s.mylayoutbox,
         myspacer,
         wibox.widget.systray(),
         myspacer,
         myspacerline,
         mytextclock,
         -- mykeyboardlayout,
         myspacerline,
         BAT0,
         myspacer,
         BAT1,
         volumecfg.widget,
--         mymic.widget,
         myspacer,
         --widget_mic,
         --backlight.widget,
    }
end

function WB.add_widgets_right (s)
    return { -- Right widgets
         layout = wibox.layout.fixed.horizontal,
         myspacer,
         s.mylayoutbox,
         myspacer,
    }
end
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

function WB.generate_wibox_one (s)
  -- layout: l_left, tasklist, l_right

  -- Create the wibox
  s.wibox_top = awful.wibar({ position = "top", screen = s, })


  s.wibox_top:setup {
		  layout = wibox.layout.align.horizontal,
		  WB.add_widgets_left (s),
		  WB.add_widgets_middle (s),
		  WB.add_widgets_right_main (s),
  }


--	if s.index == 1 then
--		-- Add widgets to the wibox
--		s.wibox_top:setup {
--		  layout = wibox.layout.align.horizontal,
--		  WB.add_widgets_left (s),
--		  WB.add_widgets_middle (s),
--		  WB.add_widgets_right_main (s),
--  	}
--  else
--  	-- Add widgets to the wibox
--		s.wibox_top:setup {
--		  layout = wibox.layout.align.horizontal,
--		  WB.add_widgets_left (s),
--		  WB.add_widgets_middle (s),
--		  WB.add_widgets_right (s),
--  	}
--	end
end

function WB.setup_common_boxes (s)
    -- Wallpaper
      set_wallpaper(s)

      -- Create a promptbox for each screen
      s.mypromptbox = awful.widget.prompt()

      -- Create an imagebox widget which will contain an icon indicating which layout we're using.
      -- We need one layoutbox per screen.
      s.mylayoutbox = awful.widget.layoutbox(s)
      s.mylayoutbox:buttons(gears.table.join(
                             awful.button({ }, 1, function () awful.layout.inc( 1) end),
                             awful.button({ }, 3, function () awful.layout.inc(-1) end),
                             awful.button({ }, 4, function () awful.layout.inc( 1) end),
                             awful.button({ }, 5, function () awful.layout.inc(-1) end)))

      -- Create a taglist widget
      s.mytaglist = awful.widget.taglist {
          screen  = s,
          filter  = awful.widget.taglist.filter.all,
          buttons = WB.taglist
      }

      -- Create a tasklist widget
      s.mytasklist = awful.widget.tasklist {
          screen  = s,
          filter  = awful.widget.tasklist.filter.currenttags,
          buttons = WB.tasklist
      }
    end

WB.taglist  = taglist
    WB.tasklist = tasklist

    --WB.initdeco()

    awful.screen.connect_for_each_screen(function(s)
        WB.setup_common_boxes(s)

        -- Create the top wibox
        WB.generate_wibox_one(s)

        -- Create the bottom wibox
        --WB.generate_wibox_two(s)

    end)

awful.spawn("compton")
awful.spawn.with_shell("autorandr -c")
awful.spawn.with_shell("xscreensaver -no-splash")
awful.spawn.with_shell("/snap/emacs/1422/usr/bin/emacs -daemon &")
awful.spawn("xfce4-power-manager")   -- allows brightness adjustment using brightness keys on keyboard

awful.spawn("xfce4-clipman")
awful.spawn("redshift-gtk")
awful.spawn.with_shell("sh -c '~/Applications/pcloud'")
awful.util.spawn("nm-applet")
awful.spawn.with_shell("kdeconnect-indicator")
awful.spawn("blueman-tray")

-- prevent power-saver from switching off devices
awful.spawn.with_shell("xset -dpms") --
awful.spawn.with_shell("xset s off")
awful.spawn.with_shell("xset s noblank")
