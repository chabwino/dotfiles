#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1

[ -d ~/bin ] && { PATH="/home/chabwino/bin:${PATH}"; }
export PATH
export PATH="$HOME/.phpenv/bin:$PATH"
eval "$(phpenv init -)"
