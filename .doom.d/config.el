;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Dominik Seeger"
      user-mail-address "dominikseeger@gmx.net")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)
(load-theme 'doom-one t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(setq auth-sources '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))

(add-to-list 'load-path "~/.doom.d/mu4e")
(require 'mu4e)

(use-package mu4e
  :ensure nil
  ;;:defer 20
  :config

  (setq mu4e-change-filenames-when-moving t)

  (setq mu4e-update-interval (* 2 60))
  (setq mu4e-get-mail-command "mbsync -c ~/.doom.d/mu4e/.mbsyncrc -a")
  (setq mu4e-maildir "~/Maildir")

  ;; Don't ask how to send email, just use emacs internal method
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; When composing an email, ask which context to use.
  ;; Alternative could be ask-if-none, this would only ask if no context is selected yet.
  (setq mu4e-compose-context-policy 'ask)

  (setq mu4e-compose-dont-reply-to-self t)
  (setq mu4e-compose-format-flowed t)
  (setq mu4e-compose-complete-addresses t)
  (setq mu4e-compose-complete-only-personal nil)
  (setq mu4e-headers-fields
        '((:human-date . 12)
         (:flags . 7)
         (:from . 25)
         (:subject)))

  (setq mu4e-contexts
        (list
         ;;gmx
         (make-mu4e-context
          :name "gmx"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/gmx" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@gmx.net")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "mail.gmx.net")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/gmx/Entw\&APw-rfe")
                  (mu4e-sent-folder . "/gmx/Gesendet")
                  (mu4e-refile-folder . "/gmx/All Mail")
                  (mu4e-trash-folder . "/gmx/Gel\&APY-scht")))

         ;;psi-main
         (make-mu4e-context
          :name "psi-main"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/psi-main" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@psicheck.app")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtppro.zoho.eu")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/psi-main/Drafts")
                  (mu4e-sent-folder . "/psi-main/Sent")
                  (mu4e-refile-folder . "/psi-main/All Mail")
                  (mu4e-trash-folder . "/psi-main/Trash")))

        ;;psi-ch
         (make-mu4e-context
          :name "psi-ch"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/psi-ch" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "dominikseeger@psicheck.ch")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtppro.zoho.eu")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type . ssl)
                  (mu4e-drafts-folder . "/psi-ch/Drafts")
                  (mu4e-sent-folder . "/psi-ch/Sent")
                  (mu4e-refile-folder . "/psi-ch/All Mail")
                  (mu4e-trash-folder . "/psi-ch/Trash")))


        ;;outlook
         (make-mu4e-context
          :name "outlook"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/outlook" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "chabwino13@outlook.com")
                  (user-full-name . "Dominik Seeger")
                  (smtpmail-smtp-server . "smtp-mail.outlook.com")
                  (smtpmail-smtp-service . 587)
                  (smtpmail-stream-type . nil)
                  (mu4e-drafts-folder . "/outlook/Drafts")
                  (mu4e-sent-folder . "/outlook/Sent")
                  (mu4e-refile-folder . "/outlook/All Mail")
                  (mu4e-trash-folder . "/outlook/Deleted")))))



  (setq mu4e-maildir-shortcuts
        '(("/gmx/INBOX" . ?g)
          ("/psi-main/INBOX" . ?p)
          ("/psi-ch/INBOX" . ?c)))

  (add-to-list 'mu4e-bookmarks '("m:/gmx/INBOX or m:/psi-main/INBOX or m:/psi-ch/INBOX or m:/outlook/INBOX" "All Inboxes" ?i)))

;;(mu4e t)

(defun efs/lookup-password (&rest keys)
  (let ((result (apply #'auth-source-search keys)))
    (if result
        (funcall (plist-get (car result) :secret))
      nil)))


(setq x-select-enable-clipboard-manager nil)


(use-package org-mime
  :ensure t
  :config
  (setq org-mime-export-options '(:section-numbers nil
                                  :with-author nil
                                  :with-toc nil))
  (add-hook 'message-send-hook 'org-mime-htmlize))
