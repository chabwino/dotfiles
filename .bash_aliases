alias ls='exa -al --color=always --group-directories-first'

# sqashfs to hide snap loops
alias df='df -h -x"squashfs"'

# warn before removing
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

alias sdn='sudo shutdown now'

# bare git repo alias for dotfiles
alias config="/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME"

# laravel
alias art='php artisan'
#alias sail='[ -f sail ] && bash sail || bash vendor/bin/sail'
#alias art='vendor/bin/sail artisan'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias ..='cd ..'

alias emacs="emacsclient -c -a 'emacs'"

alias dco='docker-compose'
alias dcu='docker-compose up -d'
alias webs='cd ~/Documents/hosttech/Projects/WSC/websitecreator'
